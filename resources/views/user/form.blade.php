@extends('layouts.base')

@section('title',$title)
@section('page_name',$title)
    
@section('content')
    <div class="row">
        <div class="col-md-7 mx-auto">
          {{-- Notification --}}
            @if ($errors->any())
              <div class="alert alert-danger alert-dismissible">
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                <h5><i class="icon fas fa-check"></i> Error</h5>
                Terjadi Kesalahan , silahkan periksa kembali inputan anda
              </div>
            @endif
            <div class="card">
                <div class="card-header">
                    <div class="card-title">
                        {{ $page_name }}
                    </div>
                </div>
                <div class="card-body">
                    <form action="{{ $is_edit ? route("users.update",@$rsUser->id) : route("users.store") }}" method="post">
                        @csrf
                        @if ($is_edit)
                          @method("PUT")
                        @endif                                                                      
                        <div class="mb-3">
                          <label for="name" class="form-label">Nama</label>
                          <input type="text" name="name" id="name" class="form-control @error('name') is-invalid @enderror" value="{{ @old("name")!="" ? @old("name") : @$rsUser->name }}">
                          @error("name")
                            <div id="valname" class="invalid-feedback">
                              {{ $message }}
                            </div>
                          @enderror
                        </div>
                        <div class="mb-3">
                          <label for="email" class="form-label">Email</label>
                          <input type="text" name="email" id="email" class="form-control @error('email') is-invalid @enderror" value="{{ @old("email")!="" ? @old("email") : @$rsUser->email }}">
                          @error("email")
                            <div id="valemail" class="invalid-feedback">
                              {{ $message }}
                            </div>
                          @enderror
                        </div>                                   
                        <div class="mb-3">
                          <label for="password" class="form-label">Password</label>
                          <input type="password" name="password" id="password" class="form-control @error('password') is-invalid @enderror" value="{{ @old("password") }}">
                          <input type="hidden" name="old_password" id="old_password" value="{{ @$rsUser->password }}">
                          @error("password")
                            <div id="valpassword" class="invalid-feedback">
                              {{ $message }}
                            </div>
                          @enderror
                        </div>
                        <div class="mb-3">
                          <label for="id_reff" class="form-label">ID Reff ( NIM / NIK )</label>
                          <input type="text" name="id_reff" id="id_reff" class="form-control @error('id_reff') is-invalid @enderror" value="{{ @old("id_reff")!="" ? @old("id_reff") : @$rsUser->id_reff }}">
                          @error("id_reff")
                            <div id="valid_reff" class="invalid-feedback">
                              {{ $message }}
                            </div>
                          @enderror
                        </div>                               
                        <div class="mb-3">
                          <label for="role" class="form-label">Role</label>
                          <select name="role" id="role" class="form-control @error('role') is-invalid @enderror">
                            <option {{ (@old("role") == "Admin" ? "selected" : (@$rsUser->role == "Admin" ? "selected" : "" )) }} value="Admin">Admin</option>
                            <option {{ (@old("role") == "Mahasiswa" ? "selected" : (@$rsUser->role == "Mahasiswa" ? "selected" : "" )) }} value="Mahasiswa">Mahasiswa</option>
                          </select>
                          @error("role")
                          <div id="valrole" class="invalid-feedback">
                            {{ $message }}
                          </div>
                          @enderror
                      </div>      
                      <div class="mb-3">
                          <label for="status" class="form-label">Status</label>
                          <select name="status" id="status" class="form-control @error('status') is-invalid @enderror">                              
                            <option {{ (@old("status") == 1 ? "selected" : (@$rsUser->status == 1 ? "selected" : "")) }} value="1">Aktif</option>
                            <option {{ (@old("status") == 2 ? "selected" : (@$rsUser->status == 2 ? "selected" : "")) }} value="2">Non Aktif</option>
                          </select>
                          @error("status")
                          <div id="valstatus" class="invalid-feedback">
                            {{ $message }}
                          </div>
                          @enderror
                      </div>                                                  
                        <div class="mb-3">
                            <input type="submit" value="SIMPAN" class="btn btn-primary btn-md">
                        </div>        
                        <div id="complete"></div>
                    </form>
                </div>
            </div>
        </div>
    </div>
@endsection