@extends('layouts.base')

@section('title',$title)
@section('page_name',$title)

@section("content")
<div class="card">
    <div class="card-header">
        <div class="card-tools">
            <a href="{{ route("users.create") }}" class="btn btn-primary btn-xs"><i class="fas fa-plus"></i> ADD NEW</a>
        </div> 
    </div>
    <div class="card-body">
        <table class="data table table-bordered table-striped">
            <thead>
                <tr>
                    <th>NAMA</th>
                    <th>EMAIL</th>
                    <th>ROLE</th>
                    <th>STATUS</th>
                    <th>ACTION</th>
                </tr>
            </thead>
            <tbody>
                @foreach ($dtUser as $rsUser)                
                    <tr>
                        <td>{{ $rsUser->name}}</td>
                        <td>{{ $rsUser->email}}</td>
                        <td>{{ $rsUser->role }}</td>
                        <td>
                            <span class="badge bg-{{ $rsUser->status==1 ? "success" : "danger" }}">{{ $rsUser->status==1 ? "Aktif" : "Non Aktif" }}</span>
                        </td>
                        <td class="text-center">
                            <a class="btn btn-warning btn-xs" href="{{ route('users.edit',$rsUser->id) }}"><i class="fas fa-edit"></i></a>
                            <form class="d-inline" action="{{ route("users.destroy",$rsUser->id) }}" method="post">
                                @method("DELETE")
                                <button type="submit" class="btn btn-danger btn-xs"><i class="fas fa-times"></i></button>
                            </form>
                        </td>
                    </tr>
                @endforeach
            </tbody>
        </table>            
    </div>
</div>   
@endsection