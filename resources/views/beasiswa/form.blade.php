@extends('layouts.base')

@section('title',$title)
@section('page_name',$title)
    
@section('content')
    <div class="row">
        <div class="col-md-7 mx-auto">
          {{-- Notification --}}
            @if ($errors->any())
              <div class="alert alert-danger alert-dismissible">
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                <h5><i class="icon fas fa-check"></i> Error</h5>
                Terjadi Kesalahan , silahkan periksa kembali inputan anda
              </div>
            @endif
            <div class="card">
                <div class="card-header">
                    <div class="card-title">
                        FORM BEASISWA
                    </div>
                </div>
                <div class="card-body">
                    <form action="{{ route("beasiswa.store") }}" method="post" enctype="multipart/form-data">
                        @csrf
                        <div class="mb-3">
                          <label for="nim_mhs" class="form-label">NIM</label>
                          <input type="text" name="nim_mhs" id="nim_mhs" class="form-control @error('nim_mhs') is-invalid @enderror" value="{{ @old("nim_mhs") ? @old("nim_mhs") : Auth::user()->id_reff }}">
                          @error("nim_mhs")
                            <div id="valnimmhs" class="invalid-feedback">
                              {{ $message }}
                            </div>
                          @enderror
                        </div>
                        <div class="mb-3">
                          <label for="nm_mhs" class="form-label">Nama</label>
                          <input type="text" name="nm_mhs" id="nm_mhs" class="form-control @error('nm_mhs') is-invalid @enderror" value="{{ @old("nm_mhs") ?  @old("nm_mhs") : Auth::user()->name }}">
                          @error("nm_mhs")
                            <div id="valnimmhs" class="invalid-feedback">
                              {{ $message }}
                            </div>
                          @enderror
                        </div>
                        <div class="mb-3">
                          <label for="email" class="form-label">Email</label>
                          <input type="email" name="email" id="email" class="form-control @error('email') is-invalid @enderror" value="{{ @old("email") ?  @old("email") : Auth::user()->email }}">
                          @error("email")
                            <div id="valemail" class="invalid-feedback">
                              {{ $message }}
                            </div>
                          @enderror
                        </div>
                        <div class="mb-3">
                          <label for="telp" class="form-label">Telepon</label>
                          <input type="text" name="telp" id="telp" class="form-control @error('telp') is-invalid @enderror" value="{{ @old("telp") }}">
                          @error("telp")
                            <div id="valtelp" class="invalid-feedback">
                              {{ $message }}
                            </div>
                          @enderror
                        </div>
                        <div class="mb-3">
                          <label for="semester" class="form-label">Semester</label>
                          <select name="semester" id="semester" class="form-control @error('semester') is-invalid @enderror" onchange="get_ipk('{{ url('/').'/api/getipk' }}')">
                            <option value="">- Pilih Semester -</option>
                            @for ($i=1;$i<=8;$i++)
                                <option {{ @old("semester") == $i ? "selected" : "" }} value="{{ $i }}">Semester {{ $i }}</option>
                            @endfor 
                          </select>
                          @error("semester")
                            <div id="valsemester" class="invalid-feedback">
                              {{ $message }}
                            </div>
                          @enderror
                        </div>
                        <div class="mb-3">
                            <label for="ipk" class="form-label">IPK</label>
                            <input type="text" name="ipk" id="ipk" class="form-control @error('ipk') is-invalid @enderror" readonly value="{{ @old("ipk") }}">
                            @error("ipk")
                            <div id="valipk" class="invalid-feedback">
                              {{ $message }}
                            </div>
                            @enderror
                        </div>
                        <div class="mb-3 comopt d-none">
                            <label for="jenis_beasiswa" class="form-label">Beasiswa</label>
                            <select name="jenis_beasiswa" id="jenis_beasiswa" class="form-control @error('jenis_beasiswa') is-invalid @enderror">
                              <option value="">- Pilih beasiswa -</option>
                              <option {{ @old("jenis_beasiswa") == "Akademik" ? "selected" : "" }} value="Akademik">Akademik</option>
                              <option {{ @old("jenis_beasiswa") == "Non Akademik" ? "selected" : "" }} value="Non Akademik">Non Akademik</option>
                            </select>
                            @error("jenis_beasiswa")
                            <div id="valjenis_beasiswa" class="invalid-feedback">
                              {{ $message }}
                            </div>
                            @enderror
                        </div>    
                        <div class="mb-3 comopt d-none">
                          <label for="" class="form-label">Upload Berkas Syarat</label>
                          <input type="file" class="form-control @error('berkas') is-invalid @enderror" name="berkas" id="berkas">
                          <div id="fileHelpId" class="form-text">File Maximal 1 mb</div>
                            @error("berkas")
                            <div id="valberkas" class="text-danger">
                              {{ $message }}
                            </div>
                            @enderror                          
                        </div>    
                        <div class="mb-3 comopt d-none">
                            <input type="submit" value="DAFTAR" class="btn btn-primary btn-md">
                        </div>        
                    </form>
                </div>
            </div>
        </div>
    </div>
@endsection