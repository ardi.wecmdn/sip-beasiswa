@extends('layouts.base')

@section('title',$title)
@section('page_name',$title)

@section("content")

{{-- Notification --}}
@if (session("text"))
    <div class="alert alert-{{ session("type") }} alert-dismissible">
        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
        {{ session("text") }}
    </div>
@endif

<div class="card">
    <div class="card-header">
        <div class="card-tools">
            <a href="{{ route("beasiswa.create") }}" class="btn btn-primary btn-xs"><i class="fas fa-plus"></i> ADD NEW</a>
        </div> 
    </div>
    <div class="card-body">
        <table class="data table table-bordered table-striped">
            <thead>
                <tr>
                    <th>TANGGAL</th>
                    <th>NIM</th>
                    <th>NAMA</th>
                    <th>SEMESTER</th>
                    <th>IPK</th>
                    <th>JENIS</th>
                    <th>STATUS</th>
                    <th>ACTION</th>
                </tr>
            </thead>
            <tbody>
                @foreach ($dtBeasiswa as $rsBeasiswa)                
                    <tr>
                        <td>{{ $rsBeasiswa->tgl_pengajuan }}</td>
                        <td>{{ $rsBeasiswa->nim_mhs }}</td>
                        <td>{{ $rsBeasiswa->nm_mhs }}</td>                       
                        <td>{{ $rsBeasiswa->semester }}</td>
                        <td>{{ $rsBeasiswa->ipk }}</td>
                        <td>{{ $rsBeasiswa->jenis_beasiswa }}</td>
                        <td>
                            <span class="badge bg-{{ ($rsBeasiswa->status==1 ? "success" : ($rsBeasiswa->status==2 ? "danger" : "warning")) }}">{{ ($rsBeasiswa->status==1 ? "Verified" : ($rsBeasiswa->status==2 ? "Ditolak" : "Belum di Verifikasi")) }}</span>
                        </td>
                        <td>
                            <button data-syarat="{{ asset('assets/syarat/'.$rsBeasiswa->syarat) }}" type="button" class="btn btn-xs btn-info" data-toggle="modal" data-target="#modal-lg" link-verifikasi="{{ route("update_status",["id"=>$rsBeasiswa->id,"status"=>1]) }}" link-reject="{{ route("update_status",["id"=>$rsBeasiswa->id,"status"=>2]) }}" onclick="view_detail('{{ $rsBeasiswa }}',this)">
                                DETAILS
                            </button>
                        </td>
                    </tr>
                @endforeach
            </tbody>
        </table>            
    </div>
</div>   


{{-- Modal --}}
<div class="modal fade" id="modal-lg">
    <div class="modal-dialog modal-lg">
      <div class="modal-content">
        <div class="modal-header">
          <h4 class="modal-title">Data Beasiswa - <span id="no_pengajuan"></span></h4>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <div class="modal-body">
            <div class="table-responsive">
                <table class="table">
                    <tbody>
                        <tr class="">
                            <td width="25%"><strong>Tanggal Pengajuan</strong></td>
                            <td width="1%">:</td>
                            <td id="tgl_pengajuan"></td>
                        </tr>
                        <tr class="">
                            <td><strong>NIM</strong></td>
                            <td>:</td>
                            <td id="nim_mhs"></td>
                        </tr>
                        <tr class="">
                            <td><strong>Nama</strong></td>
                            <td>:</td>
                            <td id="nm_mhs"></td>
                        </tr>
                        <tr class="">
                            <td><strong>Email</strong></td>
                            <td>:</td>
                            <td id="email"></td>
                        </tr>
                        <tr class="">
                            <td><strong>Telepon</strong></td>
                            <td>:</td>
                            <td id="telp"></td>
                        </tr>
                        <tr class="">
                            <td><strong>Semester</strong></td>
                            <td>:</td>
                            <td id="semester"></td>
                        </tr>
                        <tr class="">
                            <td><strong>IPK</strong></td>
                            <td>:</td>
                            <td id="ipk"></td>
                        </tr>
                        <tr class="">
                            <td><strong>Jenis Beasiswa</strong></td>
                            <td>:</td>
                            <td id="beasiswa"></td>
                        </tr>
                        <tr class="">
                            <td><strong>Berkas Syarat</strong></td>
                            <td>:</td>
                            <td>
                                <a class="btn btn-info btn-xs" href="" id="syarat" target="_blank"><i class="fas fa-eye"></i> LIHAT BERKAS</a>
                            </td>
                        </tr>
                    </tbody>
                </table>
            </div>
            
        </div>    
      </div>
      <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
  </div>
  <!-- /.modal -->
@endsection