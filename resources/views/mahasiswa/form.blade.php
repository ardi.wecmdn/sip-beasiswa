@extends('layouts.base')

@section('title',$title)
@section('page_name',$page_name)
    
@section('content')
    <div class="row">
        <div class="col-md-7 mx-auto">
          {{-- Notification --}}
            @if ($errors->any())
              <div class="alert alert-danger alert-dismissible">
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                <h5><i class="icon fas fa-check"></i> Error</h5>
                Terjadi Kesalahan , silahkan periksa kembali inputan anda
              </div>
            @endif
            <div class="card">
                <div class="card-header">
                    <div class="card-title">
                        {{ $title }}
                    </div>
                </div>
                <div class="card-body">
                    <form action="{{ $is_edit ? route("mahasiswa.update",@$rsMhs->id) : route("mahasiswa.store") }}" method="post">
                        @csrf
                        @if ($is_edit)
                          @method("PUT")
                        @endif
                        <div class="mb-3">
                          <label for="nim_mhs" class="form-label">NIM</label>
                          <input type="text" name="nim_mhs" id="nim_mhs" class="form-control @error('nim_mhs') is-invalid @enderror" value="{{ @old("nim_mhs")!="" ? @old("nim_mhs") : @$rsMhs->nim_mhs }}">
                          @error("nim_mhs")
                            <div id="valnimmhs" class="invalid-feedback">
                              {{ $message }}
                            </div>
                          @enderror
                        </div>
                        <div class="mb-3">
                          <label for="nm_mhs" class="form-label">Nama</label>
                          <input type="text" name="nm_mhs" id="nm_mhs" class="form-control @error('nm_mhs') is-invalid @enderror" value="{{ @old("nm_mhs")!="" ? @old("nm_mhs") : @$rsMhs->nm_mhs }}">
                          @error("nm_mhs")
                            <div id="valnimmhs" class="invalid-feedback">
                              {{ $message }}
                            </div>
                          @enderror
                        </div>                  
                        <div class="mb-3">
                            <label for="jur_mhs" class="form-label">Jurusan</label>
                            <select name="jur_mhs" id="jur_mhs" class="form-control @error('jur_mhs') is-invalid @enderror">
                              <option value="">- Pilih Jurusan -</option>
                              <option {{ (@old("jur_mhs") == "Teknik Informatika" ? "selected" : (@$rsMhs->jur_mhs == "Teknik Informatika" ? "selected" : "" )) }} value="Teknik Informatika">Teknik Informatika</option>
                              <option {{ (@old("jur_mhs") == "Rekayasa Sistem Komputer" ? "selected" : (@$rsMhs->jur_mhs == "Rekayasa Sistem Komputer" ? "selected" : "" )) }} value="Rekayasa Sistem Komputer">Rekayasa Sistem Komputer</option>
                              <option {{ (@old("jur_mhs") == "Akuntansi" ? "selected" : (@$rsMhs->jur_mhs == "Akuntansi" ? "selected" : "" )) }} value="Akuntansi">Akuntansi</option>
                            </select>
                            @error("jur_mhs")
                            <div id="valjur_mhs" class="invalid-feedback">
                              {{ $message }}
                            </div>
                            @enderror
                        </div>      
                        <div class="mb-3">
                            <label for="status_mhs" class="form-label">Status</label>
                            <select name="status_mhs" id="status_mhs" class="form-control @error('status_mhs') is-invalid @enderror">                              
                              <option {{ (@old("status_mhs") == 1 ? "selected" : (@$rsMhs->status_mhs == 1 ? "selected" : "")) }} value="1">Aktif</option>
                              <option {{ (@old("status_mhs") == 2 ? "selected" : (@$rsMhs->status_mhs == 2 ? "selected" : "")) }} value="2">Non Aktif</option>
                            </select>
                            @error("status_mhs")
                            <div id="valstatus_mhs" class="invalid-feedback">
                              {{ $message }}
                            </div>
                            @enderror
                        </div>      
                        <div class="mb-3">
                            <input type="submit" value="SIMPAN" class="btn btn-primary btn-md">
                        </div>        
                    </form>
                </div>
            </div>
        </div>
    </div>
@endsection