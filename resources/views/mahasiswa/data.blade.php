@extends('layouts.base')

@section('title',$title)
@section('page_name',$title)

@section("content")

{{-- Notification --}}
@if (session("text"))
    <div class="alert alert-{{ session("type") }} alert-dismissible">
        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
        {{ session("text") }}
    </div>
@endif

<div class="card">
    <div class="card-header">
        <div class="card-tools">
            <a href="{{ route("mahasiswa.create") }}" class="btn btn-primary btn-xs"><i class="fas fa-plus"></i> ADD NEW</a>
        </div> 
    </div>
    <div class="card-body">
        <table class="data table table-bordered table-striped">
            <thead>
                <tr>
                    <th>NIM</th>
                    <th>NAMA</th>
                    <th>JURUSAN</th>
                    <th>STATUS</th>
                    <th>ACTION</th>
                </tr>
            </thead>
            <tbody>
                @foreach ($dtMhs as $rsMhs)                
                    <tr>
                        <td>{{ $rsMhs->nim_mhs }}</td>
                        <td>{{ $rsMhs->nm_mhs }}</td>
                        <td>{{ $rsMhs->jur_mhs }}</td>
                        <td>
                            <span class="badge bg-{{ $rsMhs->status_mhs==1 ? "success" : "danger" }}">{{ $rsMhs->status_mhs==1 ? "Aktif" : "Non Aktif" }}</span>
                        </td>
                        <td class="text-center">
                            <a class="btn btn-warning btn-xs" href="{{ route('mahasiswa.edit',$rsMhs->id) }}"><i class="fas fa-edit"></i></a>
                            <form class="d-inline" action="{{ route("mahasiswa.destroy",$rsMhs->id) }}" method="post">
                                @csrf
                                @method("DELETE")
                                <button type="submit" class="btn btn-danger btn-xs"><i class="fas fa-times"></i></button>
                            </form>
                        </td>
                    </tr>
                @endforeach
            </tbody>
        </table>            
    </div>
</div>   
@endsection