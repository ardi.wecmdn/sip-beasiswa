<!DOCTYPE html>
<!--
This is a starter template page. Use this page to start your new project from
scratch. This page gets rid of all links and provides the needed markup only.
-->
<html lang="en">
<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <title>@yield("title") | SISTEM BEASISWA</title>
  @include("layouts.sc_head")
</head> 
<body class="hold-transition sidebar-mini">
<div class="wrapper">

  @include("layouts.navbar")

  @include("layouts.sidebar")

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <div class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1 class="m-0">@yield("page_name")</h1>
          </div><!-- /.col -->         
        </div><!-- /.row -->
      </div><!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->

    <!-- Main content -->
    <div class="content">
      <div class="container-fluid">
        @yield('content')
      </div><!-- /.container-fluid -->
    </div>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->


  <!-- Main Footer -->
  <footer class="main-footer">
    <!-- To the right -->
    <div class="float-right d-none d-sm-inline">
      ARDI YOTO
    </div>
    <!-- Default to the left -->
    <strong>Copyright &copy; @php date("Y") @endphp <a href="https://ardiyoto.my.id">ARDI YOTO</a>.</strong> All rights reserved.
  </footer>
</div>
<!-- ./wrapper -->

@include("layouts.sc_footer")
</body>
</html>
