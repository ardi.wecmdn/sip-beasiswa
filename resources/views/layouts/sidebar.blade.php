<!-- Main Sidebar Container -->
  <aside class="main-sidebar sidebar-dark-primary elevation-4">
    <!-- Brand Logo -->
    <a href="{{ route("home") }}" class="brand-link text-center">     
      <span class="brand-text font-weight-bold">SIP BEASISWA</span>
    </a>

    <!-- Sidebar -->
    <div class="sidebar">
      <!-- Sidebar user panel (optional) -->
      <div class="user-panel mt-3 pb-3 mb-3 d-flex">
        <div class="info">
          <a href="#" class="d-block">{{ @Auth::user()->name }} <br/> <small>( {{ @Auth::user()->role }} )</small></a>
        </div>
      </div>

      <!-- Sidebar Menu -->
      <nav class="mt-2">
        <ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu" data-accordion="false">
          <!-- Add icons to the links using the .nav-icon class
               with font-awesome or any other icon font library -->          
          <li class="nav-item">
            <a href="{{ route("home") }}" class="nav-link">
              <i class="nav-icon fas fa-home"></i>
              <p>
                Dashboard
              </p>
            </a>
          </li>
          @if(Auth::user()->role=="Admin")
          <li class="nav-item">
            <a href="{{ route("mahasiswa.index") }}" class="nav-link {{ request()->is(['mahasiswa','mahasiswa/*']) ? 'active' : '' }}">
              <i class="nav-icon fas fa-user-graduate"></i>
              <p>
                Mahasiswa
              </p>
            </a>
          </li>
          <li class="nav-item">
            <a href="{{ route("nilai.index") }}" class="nav-link {{ request()->is(['nilai','nilai/*']) ? 'active' : '' }}">
              <i class="nav-icon fas fa-star"></i>
              <p>
                Nilai
              </p>
            </a>
          </li>
          @endif
          <li class="nav-item">
            <a href="{{ route("beasiswa.index") }}" class="nav-link {{ request()->is(['beasiswa','beasiswa/*']) ? 'active' : '' }}">
              <i class="nav-icon fas fa-dollar-sign"></i>
              <p>
                Beasiswa
              </p>
            </a>
          </li>          
          @if(Auth::user()->role=="Admin")
          <li class="nav-item">
            <a href="{{ route("users.index") }}" class="nav-link {{ request()->is(['users','users/*']) ? 'active' : '' }}">
              <i class="nav-icon fas fa-users"></i>
              <p>
                User
              </p>
            </a>
          </li>          
          @endif
          <li class="nav-item">
            <a href="{{ route("auth.logout") }}" class="nav-link text-danger">
              <i class="nav-icon fas fa-sign-out-alt"></i>              
                Logout
              </p>
            </a>
          </li>          
        </ul>
      </nav>
      <!-- /.sidebar-menu -->
    </div>
    <!-- /.sidebar -->
  </aside>