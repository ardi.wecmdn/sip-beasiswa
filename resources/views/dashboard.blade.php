@extends('layouts.base')

@section('title','Dashboard')
@section('page_name','Dashboard')
    
@section('content')
<!-- Small boxes (Stat box) -->
{{-- Notification --}}
@if (session("text"))
    <div class="alert alert-{{ session("type") }} alert-dismissible">
        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
        {{ session("text") }}
    </div>
@endif
<div class="row">
    <div class="col-lg-3 col-6">
      <!-- small box -->
      <div class="small-box bg-info">
        <div class="inner">
          <h3>{{ $rsJPemohon[0]->jumlah }}</h3>

          <p>Permohonan</p>
        </div>
        <div class="icon">
          <i class="ion ion-bag"></i>
        </div>        
      </div>
    </div>
    <!-- ./col -->
    <div class="col-lg-3 col-6">
      <!-- small box -->
      <div class="small-box bg-warning">
        <div class="inner">
            <h3>{{ $rsJProses[0]->jumlah }}</h3>

          <p>Permohonan Baru</p>
        </div>
        <div class="icon">
          <i class="ion ion-stats-bars"></i>
        </div>        
      </div>
    </div>
    <!-- ./col -->
    <div class="col-lg-3 col-6">
      <!-- small box -->
      <div class="small-box bg-success">
        <div class="inner">
          <h3>{{ $rsJVer[0]->jumlah }}</h3>

          <p>Terverifikasi</p>
        </div>
        <div class="icon">
          <i class="ion ion-person-add"></i>
        </div>        
      </div>
    </div>
    <!-- ./col -->
    <div class="col-lg-3 col-6">
      <!-- small box -->
      <div class="small-box bg-danger">
        <div class="inner">
          <h3>{{ $rsJTolak[0]->jumlah }}</h3>

          <p>Ditolak</p>
        </div>
        <div class="icon">
          <i class="ion ion-pie-graph"></i>
        </div>        
      </div>
    </div>
    <!-- ./col -->
  </div>
  <!-- /.row -->
@endsection