@extends('layouts.base')

@section('title',$title)
@section('page_name',$title)

@section("content")
{{-- Notification --}}
@if (session("text"))
    <div class="alert alert-{{ session("type") }} alert-dismissible">
        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
        {{ session("text") }}
    </div>
@endif
<div class="card">
    <div class="card-header">
        <div class="card-tools">
            <a href="{{ route("nilai.create") }}" class="btn btn-primary btn-xs"><i class="fas fa-plus"></i> ADD NEW</a>
        </div> 
    </div>
    <div class="card-body">
        <table class="data table table-bordered table-striped">
            <thead>
                <tr>
                    <th>NIM</th>
                    <th>NAMA</th>
                    <th>SEMESTER</th>
                    <th>IPK</th>
                    <th>ACTION</th>
                </tr>
            </thead>
            <tbody>
                @foreach ($dtNilai as $rsNilai)                
                    <tr>
                        <td>{{ $rsNilai->nim_mhs}}</td>
                        <td>{{ $rsNilai->nm_mhs}}</td>
                        <td>Semester {{ $rsNilai->semester}}</td>
                        <td>{{ $rsNilai->ipk }}</td>
                        <td class="text-center">
                            <a class="btn btn-warning btn-xs" href="{{ route('mahasiswa.edit',$rsNilai->id) }}"><i class="fas fa-edit"></i></a>
                            <form class="d-inline" action="{{ route("mahasiswa.destroy",$rsNilai->id) }}" method="post">
                                @method("DELETE")
                                <button type="submit" class="btn btn-danger btn-xs"><i class="fas fa-times"></i></button>
                            </form>
                        </td>
                    </tr>
                @endforeach
            </tbody>
        </table>            
    </div>
</div>   
@endsection