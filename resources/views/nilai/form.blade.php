@extends('layouts.base')

@section('title',$title)
@section('page_name',$title)
    
@section('content')
    <div class="row">
        <div class="col-md-7 mx-auto">
          {{-- Notification --}}
            @if ($errors->any())
              <div class="alert alert-danger alert-dismissible">
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                <h5><i class="icon fas fa-check"></i> Error</h5>
                Terjadi Kesalahan , silahkan periksa kembali inputan anda
              </div>
            @endif
            <div class="card">
                <div class="card-header">
                    <div class="card-title">
                        {{ $page_name }}
                    </div>
                </div>
                <div class="card-body">
                    <form action="{{ $is_edit ? route("nilai.update",@$rsMhs->id) : route("nilai.store") }}" method="post">
                        @csrf
                        @if ($is_edit)
                          @method("PUT")
                        @endif                                                                      
                        <div class="mb-3">
                          <label for="nim_mhs" class="form-label">NIM</label>
                          <select class="form-control select2" style="width: 100%;" name="nim_mhs" onchange="get_nilai_detail(this,'{{ url('/').'/api/getnilai' }}')">
                              <option value="">- Pilih Mahasiswa -</option>
                              @foreach ($dtMhs as $rsMhs)
                                <option value="{{ $rsMhs->nim_mhs }}">{{ $rsMhs->nim_mhs." | ". $rsMhs->nm_mhs }}</option>
                              @endforeach                            
                          </select>
                        </div>
                        <div class="mb-3 comopt d-none">
                          <label for="semester" class="form-label">Semester</label>
                          <select class="form-control" name="semester" id="semester">
                          </select>
                        </div>
                        <div class="mb-3 comopt d-none">
                          <label for="ipk" class="form-label">IPK</label>
                          <input type="number" name="ipk" id="ipk" class="form-control @error('ipk') is-invalid @enderror" value="{{ @old("ipk")!="" ? @old("ipk") : @$rsNilai->ipk }}" step="0.01">
                          @error("ipk")
                            <div id="valnimmhs" class="invalid-feedback">
                              {{ $message }}
                            </div>
                          @enderror
                        </div>                                        
                        <div class="mb-3 comopt d-none">
                            <input type="submit" value="SIMPAN" class="btn btn-primary btn-md">
                        </div>        
                        <div id="complete"></div>
                    </form>
                </div>
            </div>
        </div>
    </div>
@endsection