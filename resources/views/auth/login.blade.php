<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <title>AdminLTE 3 | Log in (v2)</title>

  @include('layouts.sc_head')
</head>
<body class="hold-transition login-page">
<div class="login-box">
    {{-- Notification --}}
    @if (session("text"))
    <div class="alert alert-{{ session("type") }} alert-dismissible">                  
      <p>{{ session("text") }}</p>
    </div>
    @endif  
  <!-- /.login-logo -->
  <div class="card card-outline card-primary">
    <div class="card-header text-center">
      <a href="javascript:void(0)" class="h1"><b>SIP</b>Beasiswa</a>
    </div>
    <div class="card-body">
          {{-- Notification --}}
        @if ($errors->any())
          <div class="alert alert-danger alert-dismissible">            
            <p>Login Gagal !</p>
          </div>            
        @endif

      <form action="{{ route('auth.check') }}" method="post">
        @csrf
        <div class="input-group mb-3">
          <input type="email" class="form-control @error('email') is-invalid @enderror" name="email" placeholder="Email">
          <div class="input-group-append">
            <div class="input-group-text">
              <span class="fas fa-envelope"></span>
            </div>
          </div>
          @error("email")
          <div id="valemail" class="invalid-feedback">
            {{ $message }}
          </div>
          @enderror
        </div>
        <div class="input-group mb-3">
          <input type="password" class="form-control @error('password') is-invalid @enderror" name="password" placeholder="Password">
          <div class="input-group-append">
            <div class="input-group-text">
              <span class="fas fa-lock"></span>
            </div>
          </div>
          @error("password")
          <div id="valpassword" class="invalid-feedback">
            {{ $message }}
          </div>
          @enderror
        </div>
        <div class="row">
          <div class="col-8">
          </div>
          <!-- /.col -->
          <div class="col-4">
            <button type="submit" class="btn btn-primary btn-block">Sign In</button>
          </div>
          <!-- /.col -->
        </div>
      </form>
    </div>
    <!-- /.card-body -->
  </div>
  <!-- /.card -->
</div>
<!-- /.login-box -->

<!-- jQuery -->
@include('layouts.sc_footer')
</body>
</html>
