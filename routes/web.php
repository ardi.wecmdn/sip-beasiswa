<?php

use App\Http\Controllers\AuthCtrl;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\UserController;
use App\Http\Controllers\NilaiController;
use App\Http\Controllers\BeasiswaController;
use App\Http\Controllers\DashboardController;
use App\Http\Controllers\MahasiswaController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "web" middleware group. Make something great!
|
*/

Route::group(["middleware"=>"auth"],function(){
    Route::get('/', [DashboardController::class,'index'])->name("home");
    
    Route::group(["middleware"=>"roleAdmin"],function(){
        Route::resource('mahasiswa',MahasiswaController::class);
        Route::resource('nilai',NilaiController::class);
        Route::resource('users',UserController::class);
        Route::resource('beasiswa',BeasiswaController::class);
        Route::get('beasiswa/{id}/{status}/update',[BeasiswaController::class,'update_status'])->name("update_status");
    });

    Route::group(["middleware"=>"roleMahasiswa"],function(){
        Route::resource('beasiswa',BeasiswaController::class);
    });

    Route::get('logout',[AuthCtrl::class,'logout'])->name("auth.logout");

});

// Route Login
Route::get('login',[AuthCtrl::class,'login'])->name("auth.login");
Route::post('ceklogin',[AuthCtrl::class,'cek_login'])->name("auth.check");
