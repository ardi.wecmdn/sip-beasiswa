<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('beasiswas', function (Blueprint $table) {
            $table->id();
            $table->string("no_pengajuan");
            $table->date("tgl_pengajuan");
            $table->string("nim_mhs");
            $table->string("nm_mhs");
            $table->string("email");
            $table->string("telp");
            $table->integer("semester");
            $table->float("ipk");
            $table->string("jenis_beasiswa");
            $table->string("syarat");
            $table->integer("status");
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('beasiswas');
    }
};
