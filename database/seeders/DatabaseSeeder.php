<?php

namespace Database\Seeders;

// use Illuminate\Database\Console\Seeds\WithoutModelEvents;

use App\Models\Mahasiswa;
use App\Models\Nilai;
use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     */
    public function run(): void
    {
        // \App\Models\User::factory(10)->create();

        // \App\Models\User::factory()->create([
        //     'name' => 'Test User',
        //     'email' => 'test@example.com',
        // ]);

        User::create([
            "name"=>"Ardi Yoto",
            "email"=>"zotone@gmail.com",
            "password"=>Hash::make("123"),
            "role"=>"Admin",
            "id_reff"=>"00000000",
            "status"=>1
        ]);

        Mahasiswa::factory(10)->create();
        $ipk = [2.8,2.9,3,3.24,3.25,3.5,3.65,3.7,3.85,3.9,4];
        $mhs = Mahasiswa::All();
        foreach($mhs as $Mhs){
            for($i = 1;$i<=8;$i++){
                Nilai::create([
                    "nim_mhs" => $Mhs->nim_mhs,
                    "semester" => $i,
                    "ipk" => $ipk[rand(0,count($ipk)-1)]
                ]);
            }            
        }

    }
}
