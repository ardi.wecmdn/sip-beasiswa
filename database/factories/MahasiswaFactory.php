<?php

namespace Database\Factories;

use Illuminate\Database\Eloquent\Factories\Factory;

/**
 * @extends \Illuminate\Database\Eloquent\Factories\Factory<\App\Models\Mahasiswa>
 */
class MahasiswaFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array<string, mixed>
     */
    public function definition(): array
    {
        $jurusan = ["Teknik Informatika","Rekayasa Sistem Komputer","Akuntansi"];

        return [
            "nim_mhs" => fake('id_ID')->ean8(),
            "nm_mhs" => fake('id_ID')->name(),
            "jur_mhs" => $jurusan[rand(0,2)],
            "status_mhs" => 1
        ];
    }
}
