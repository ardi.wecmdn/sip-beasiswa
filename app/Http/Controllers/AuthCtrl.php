<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Routing\Controller;
use Illuminate\Support\Facades\Auth;

class AuthCtrl extends Controller
{
    function login(){
        // Jika sudah login akan langsung ke Dashboard
        if(Auth::check()){
            return redirect("/");
        }
        
        return view('auth.login');
    }

    function cek_login(Request $req){
        // Validasi
        $req->validate(
            [
                "email" => "required",
                "password" => "required"
            ],
            [
                "email.required" => "Maaf email harus diisi !",
                "password.required" => "Maaf password harus diisi !"
            ]
        );

        // Cek Login
        if(Auth::attempt(['email' => $req->email, 'password' => $req->password])){
            $req->session()->regenerate();
            return redirect('/'); // Dashboard
        }

        // Jika user dan password salah maka Kembali ke form Login
        $mess = [
            "type" => "danger",
            "text" => "Maaf username atau password salah !"
        ];

        return back()->with($mess);
    }   

    function logout(Request $req){
        Auth::logout();
        $req->session()->invalidate();
        $req->session()->regenerateToken();
        return redirect('login');
    }

}
