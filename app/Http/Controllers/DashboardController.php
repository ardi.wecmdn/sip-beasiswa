<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class DashboardController extends Controller
{
    function index(){
        $data = [
            "rsJPemohon" => DB::select("SELECT count(*) as jumlah FROM beasiswas"),
            "rsJProses" => DB::select("SELECT count(*) as jumlah FROM beasiswas WHERE status = 0"),
            "rsJVer" => DB::select("SELECT count(*) as jumlah FROM beasiswas WHERE status = 1"),
            "rsJTolak" => DB::select("SELECT count(*) as jumlah FROM beasiswas WHERE status = 2"),
        ];

        return view('dashboard',$data);

    }
}
