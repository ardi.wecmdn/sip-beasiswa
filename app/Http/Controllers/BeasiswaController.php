<?php

namespace App\Http\Controllers;

use Exception;
use PDOException;
use App\Models\Beasiswa;
use Illuminate\Support\Str;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Http\Requests\StoreBeasiswaRequest;
use App\Http\Requests\UpdateBeasiswaRequest;

class BeasiswaController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    private $title = "Beasiswa";
    public function index()
    {
        $data = [
            "title" => $this->title,
            "dtBeasiswa" => Auth::user()->role=="Admin" ? Beasiswa::All() : Beasiswa::where("nim_mhs",Auth::user()->id_reff)->get()
        ];

        return view(Auth::user()->role=="Admin" ? 'beasiswa.data' : 'beasiswa.data_mahasiswa',$data);
    }

    /**
     * Show the form for creating a new resource.
     */
    public function create()
    {
        $data = [
            "title" => $this->title
        ];

        return view("beasiswa.form",$data);
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(StoreBeasiswaRequest $request)
    {

        try {            

            // File
            $filename=Str::random(20).".".$request->berkas->extension();
        
            if($request->berkas->move(public_path('assets/syarat'),$filename)){
                Beasiswa::create([
                    "no_pengajuan" => strtoupper(Str::random(10)),
                    "tgl_pengajuan"=>date("Y-m-d"),
                    "nim_mhs"=>$request->input("nim_mhs"),
                    "nm_mhs"=>$request->input("nm_mhs"),
                    "email"=>$request->input("email"),
                    "telp"=>$request->input("telp"),
                    "semester"=>$request->input("semester"),
                    "ipk"=>$request->input("ipk"),
                    "jenis_beasiswa"=>$request->input("jenis_beasiswa"),
                    "syarat"=>$filename,
                    "status"=>0
                ]);
            }

            // Message
            $mess = [
                "type" => "success",
                "text" => "Data Berhasil Disimpan !"
            ];

            return redirect(route("beasiswa.index"))->with($mess);

        } catch(Exception $err){
            $mess = [
                "type" => "danger",
                "text" => "Data Gagal Disimpan !<br/>".$err->getMessage()
            ];
            return redirect(route("beasiswa.index"))->with($mess);
        }
    }

    /**
     * Display the specified resource.
     */
    public function show(Beasiswa $beasiswa)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     */
    public function edit(Beasiswa $beasiswa)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(UpdateBeasiswaRequest $request, Beasiswa $beasiswa)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(Beasiswa $beasiswa)
    {
        //
    }

    public function update_status(Request $request){
        try {            

            Beasiswa::where("id",$request->id)->update([
                "status" => $request->status
            ]);

            // Message
            $mess = [
                "type" => "success",
                "text" => "Data Berhasil Di Update !"
            ];

            return redirect(route("beasiswa.index"))->with($mess);

        } catch(Exception $err){
            $mess = [
                "type" => "danger",
                "text" => "Data Gagal Di Update !<br/>".$err->getMessage()
            ];
            return redirect(route("beasiswa.index"))->with($mess);
        }
    }
}
