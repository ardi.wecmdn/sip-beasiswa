<?php

namespace App\Http\Controllers;

use App\Models\Nilai;
use App\Models\Mahasiswa;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class APIController extends Controller
{
    function get_ipk(Request $request){
        $ipk = Nilai::where("nim_mhs",$request->nim)
        ->where("semester",$request->semester)->first();

        return $ipk->toJson();
    }

    function get_nilai_detail(Request $request){
        $nilai = DB::table("nilais")
        ->where("nim_mhs",$request->nim)
        ->select("semester")
        ->get();

        $semester = [];
        foreach($nilai as $value){
            $semester[] = $value->semester;
        }

        $newsemester = array_diff([1,2,3,4,5,6,7,8],$semester);

        return json_encode($newsemester);
    }
}
