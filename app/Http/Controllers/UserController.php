<?php

namespace App\Http\Controllers;

use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;

class UserController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public $title = "User";
    public function index()
    {
        $data = [
            "title" => "Data ".$this->title,
            "page_name" => "Data ".$this->title,
            "dtUser" => User::All()
        ];

        return view("user.data",$data);
    }

    /**
     * Show the form for creating a new resource.
     */
    public function create()
    {
        $data = [
            "title" => "Data ".$this->title,
            "page_name" => "Add ".$this->title,
            "is_edit" => false
        ];

        return view("user.form",$data);
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(Request $request)
    {
        try {            
    
            User::create([
                "name"=>$request->input("name"),
                "email"=>$request->input("email"),
                "password"=>Hash::make($request->input("password")),
                "role"=>$request->input("role"),
                "id_reff"=>$request->input("id_reff"),
                "status"=>$request->input("status")
            ]);
   
            // Message
            $mess = [
                "type" => "success",
                "text" => "Data Berhasil Disimpan !"
            ];

            return redirect(route("users.index"))->with($mess);

        } catch(Exception $err){
            $mess = [
                "type" => "danger",
                "text" => "Data Gagal Disimpan !<br/>".$err->getMessage()
            ];
            return redirect(route("users.index"))->with($mess);
        }
    }

    /**
     * Display the specified resource.
     */
    public function show(User $user)
    {

    }

    /**
     * Show the form for editing the specified resource.
     */
    public function edit(User $user)
    {
        $data = [
            "title" => "Data ".$this->title,
            "page_name" => "Add ".$this->title,
            "rsUser" => $user,
            "is_edit" => true
        ];

        return view("user.form",$data);
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(Request $request, User $user)
    {
        try {            
    
            User::where("id",$user->id)->update([
                "name"=>$request->input("name"),
                "email"=>$request->input("email"),
                "password"=>$request->input("password") !="" ? Hash::make($request->input("password")) : $request->input("old_password"),
                "role"=>$request->input("role"),
                "id_reff"=>$request->input("id_reff"),
                "status"=>$request->input("status")
            ]);
   
            // Message
            $mess = [
                "type" => "success",
                "text" => "Data Berhasil di Update !"
            ];

            return redirect(route("users.index"))->with($mess);

        } catch(Exception $err){
            $mess = [
                "type" => "danger",
                "text" => "Data Gagal di Update !<br/>".$err->getMessage()
            ];
            return redirect(route("users.index"))->with($mess);
        }
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(User $user)
    {
        try {            
    
            User::where("id",$user->id)->delete();
   
            // Message
            $mess = [
                "type" => "success",
                "text" => "Data Berhasil di Hapus !"
            ];

            return redirect(route("mahasiswa.index"))->with($mess);

        } catch(Exception $err){
            $mess = [
                "type" => "danger",
                "text" => "Data Gagal di Hapus !<br/>".$err->getMessage()
            ];
            return redirect(route("mahasiswa.index"))->with($mess);
        }
    }
}
