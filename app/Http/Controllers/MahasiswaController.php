<?php

namespace App\Http\Controllers;

use Exception;
use App\Models\Mahasiswa;
use App\Http\Requests\StoreMahasiswaRequest;
use App\Http\Requests\UpdateMahasiswaRequest;

class MahasiswaController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    private $title = "Mahasiswa";
    public function index()
    {
        $data = [
            "title" => "Data ".$this->title,
            "dtMhs" => Mahasiswa::All()
        ];

        return view('mahasiswa.data',$data);
    }

    /**
     * Show the form for creating a new resource.
     */
    public function create()
    {
        $data = [
            "title" => "Add ".$this->title,
            "page_name" => "Form ".$this->title,
            "is_edit" => false
        ];

        return view('mahasiswa.form',$data);
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(StoreMahasiswaRequest $request)
    {

        try {            
    
            Mahasiswa::create([
                "nim_mhs"=>$request->input("nim_mhs"),
                "nm_mhs"=>$request->input("nm_mhs"),
                "jur_mhs"=>$request->input("jur_mhs"),
                "status_mhs"=>$request->input("status_mhs")
            ]);
   
            // Message
            $mess = [
                "type" => "success",
                "text" => "Data Berhasil Disimpan !"
            ];

            return redirect(route("mahasiswa.index"))->with($mess);

        } catch(Exception $err){
            $mess = [
                "type" => "danger",
                "text" => "Data Gagal Disimpan !<br/>".$err->getMessage()
            ];
            return redirect(route("mahasiswa.index"))->with($mess);
        }
    }

    /**
     * Display the specified resource.
     */
    public function show(Mahasiswa $mahasiswa)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     */
    public function edit(Mahasiswa $mahasiswa)
    {
        $data = [
            "title" => "Edit ".$this->title,
            "page_name" => "Form ".$this->title,
            "rsMhs" => $mahasiswa,
            "is_edit" => true
        ];

        return view('mahasiswa.form',$data);
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(UpdateMahasiswaRequest $request, Mahasiswa $mahasiswa)
    {
        try {            
    
            Mahasiswa::where("id",$mahasiswa->id)->update([
                "nim_mhs"=>$request->input("nim_mhs"),
                "nm_mhs"=>$request->input("nm_mhs"),
                "jur_mhs"=>$request->input("jur_mhs"),
                "status_mhs"=>$request->input("status_mhs")
            ]);
   
            // Message
            $mess = [
                "type" => "success",
                "text" => "Data Berhasil di Update !"
            ];

            return redirect(route("mahasiswa.index"))->with($mess);

        } catch(Exception $err){
            $mess = [
                "type" => "danger",
                "text" => "Data Gagal di Update !<br/>".$err->getMessage()
            ];
            return redirect(route("mahasiswa.index"))->with($mess);
        }
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(Mahasiswa $mahasiswa)
    {
        try {            
    
            Mahasiswa::where("id",$mahasiswa->id)->delete();
   
            // Message
            $mess = [
                "type" => "success",
                "text" => "Data Berhasil di Hapus !"
            ];

            return redirect(route("mahasiswa.index"))->with($mess);

        } catch(Exception $err){
            $mess = [
                "type" => "danger",
                "text" => "Data Gagal di Hapus !<br/>".$err->getMessage()
            ];
            return redirect(route("mahasiswa.index"))->with($mess);
        }
    }
}
