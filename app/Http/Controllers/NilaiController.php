<?php

namespace App\Http\Controllers;

use Exception;
use App\Models\Nilai;
use App\Models\Mahasiswa;
use Illuminate\Support\Facades\DB;
use App\Http\Requests\StoreNilaiRequest;
use App\Http\Requests\UpdateNilaiRequest;

class NilaiController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    private $title = "Nilai";
    public function index()
    {
        $data = [
            "title" => "Data ".$this->title,
            "dtNilai" => DB::table("nilais")
            ->join("mahasiswas","nilais.nim_mhs","=","mahasiswas.nim_mhs")
            ->select("nilais.*","mahasiswas.nm_mhs")
            ->get()
        ];

        return view("nilai.data",$data);
    }

    /**
     * Show the form for creating a new resource.
     */
    public function create()
    {
        $data = [
            "title" => "Form ".$this->title,
            "page_name" => "Add ".$this->title,
            "is_edit" => false,
            "dtMhs" => Mahasiswa::where("status_mhs",1)->get()
        ];

        return view("nilai.form",$data);
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(StoreNilaiRequest $request)
    {        
        try {            
    
            Nilai::create([
                "nim_mhs"=>$request->input("nim_mhs"),
                "semester"=>$request->input("semester"),
                "ipk"=>$request->input("ipk"),
            ]);
   
            // Message
            $mess = [
                "type" => "success",
                "text" => "Data Berhasil Disimpan !"
            ];

            return redirect(route("nilai.index"))->with($mess);

        } catch(Exception $err){
            $mess = [
                "type" => "danger",
                "text" => "Data Gagal Disimpan !<br/>".$err->getMessage()
            ];
            return redirect(route("nilai.index"))->with($mess);
        }
    }

    /**
     * Display the specified resource.
     */
    public function show(Nilai $nilai)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     */
    public function edit(Nilai $nilai)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(UpdateNilaiRequest $request, Nilai $nilai)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(Nilai $nilai)
    {
        //
    }
}
