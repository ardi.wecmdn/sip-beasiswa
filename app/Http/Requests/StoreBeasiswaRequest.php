<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class StoreBeasiswaRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     */
    public function authorize(): bool
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, \Illuminate\Contracts\Validation\ValidationRule|array<mixed>|string>
     */
    public function rules(): array
    {
        return [
            "nim_mhs"=>"required|numeric|digits:8",
            "nm_mhs"=>"required|max:50",
            "email"=>"required|email",
            "telp"=>"required|numeric|digits_between:12,13",
            "semester"=>"required",
            "ipk"=>"required",
            "jenis_beasiswa"=>"required",
            "berkas"=>"required|extensions:pdf|max:1000"        
        ];
    }

    public function messages(): array
    {
        return [
            "nim_mhs.required"=>"NIM Wajib diisi !",
            "nim_mhs.numeric"=>"NIM Harus Angka !",
            "nm_mhs.required"=>"Nama Wajib diisi !",
            "nm_mhs.max"=>"Nama Maximal 50 Karakter !",
            "email.required"=>"Email Wajib diisi !",
            "email.email"=>"Email Tidak Valid !",
            "telp.required"=>"Telepon Wajib diisi",
            "telp.numeric"=>"Telepon Harus Angka",
            "telp.digits_between"=>"Telepon 12 digits atau 13 digits",
            "semester.required"=>"Semester Wajib diisi !",
            "ipk.required"=>"IPK Wajib diisi !",
            "jenis_beasiswa.required"=>"Jenis Beasiswa Wajib dipilih",
            "berkas.required"=>"Berkas Wajib diisi !",
            "berkas.extensions"=>"Berkas Hanya Boleh PDF !",
            "berkas.max"=>"Berkas Maximal 1 Megabyte"
        ];
    }
}
