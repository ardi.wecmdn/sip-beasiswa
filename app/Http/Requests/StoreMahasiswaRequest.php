<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class StoreMahasiswaRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     */
    public function authorize(): bool
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, \Illuminate\Contracts\Validation\ValidationRule|array<mixed>|string>
     */
    public function rules(): array
    {
        return [
            "nim_mhs"=>"required|numeric|digits:8|unique:mahasiswas",
            "nm_mhs"=>"required|max:50",
            "jur_mhs"=>"required",    
        ];
    }

    public function messages(): array
    {
        return [
            "nim_mhs.required"=>"NIM Wajib diisi !",
            "nim_mhs.numeric"=>"NIM Harus Angka !",
            "nim_mhs.digits"=>"NIM Harus 8 digits !",
            "nim_mhs.unique"=>"NIM Sudah digunakan !",
            "nm_mhs.required"=>"Nama Wajib diisi !",
            "nm_mhs.max"=>"Nama Maximal 50 Karakter !",
            "jur_mhs.required"=>"Jurusan Wajib diisi !",
        ];
    }
}
