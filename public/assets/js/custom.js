// jQuery Custom

$(document).ready(function(){
    $(".data").DataTable({
        responsive:true,
        "autoWidth": false
    });

    if($("#ipk").length>0){
        cek_ipk(parseFloat($("#ipk").val()));
    }

    $(".select2").select2();
});

function view_detail(beasiswa,e){
    $bea = JSON.parse(beasiswa);
    console.log($bea)

    $("#no_pengajuan").html($bea.no_pengajuan)
    $("#tgl_pengajuan").html($bea.tgl_pengajuan)
    $("#nim_mhs").html($bea.nim_mhs)
    $("#nm_mhs").html($bea.nm_mhs)
    $("#email").html($bea.email)
    $("#telp").html($bea.telp)
    $("#semester").html($bea.semester)
    $("#ipk").html($bea.ipk)
    $("#beasiswa").html($bea.jenis_beasiswa)
    $("#syarat").attr("href",$(e).attr("data-syarat"))
    $("#link-verifikasi").attr("href",$(e).attr("link-verifikasi"))
    $("#link-reject").attr("href",$(e).attr("link-reject"))
}

function get_ipk(url){
    $nim = $("#nim_mhs").val();
    $semester = $("#semester").val();
    if($nim!="" && $semester!=""){
        $.get(url+"/"+$nim+"/"+$semester,function(data){
            $nilai = JSON.parse(data);
            $("#ipk").val($nilai.ipk);
            cek_ipk(parseFloat($nilai.ipk));
            console.log(data);
        });
    } else {
        if($nim==""){
            alert("Maaf NIM Harus diisi !")
        }
        if($semester==""){
            alert("Maaf Semester Harus dipilih !")
        }
    }
}

function cek_ipk(ipk){
    if(ipk>=3){
        $(".comopt").removeClass("d-none")
    } else {
        $(".comopt").addClass("d-none")
    }
}

function get_nilai_detail(e,url){
    $.get(url+"/"+$(e).val(),function(data){
        $nilai = JSON.parse(data)
        $nilai = Object.keys($nilai).map((key) => $nilai[key])
        $("#semester").html("");
        $("#complete").html("");
        if($nilai.length>0){
            $.each($nilai,function(key,val){
                $("#semester").append("<option value='"+val+"'>Semester "+val+"</option>");
            });
            $(".comopt").removeClass("d-none");
        } else {
            $(".comopt").addClass("d-none");
            $mess = '<div class="alert alert-info alert-dismissable">';
            $mess += '<button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>';
            $mess += '<h5><i class="icon fas fa-check"></i> Complete</h5>';
            $mess += 'Semua Nilai sudah di Inputkan !';
            $mess += '</div>';
            $("#complete").html($mess);
        }
    });
}